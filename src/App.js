import React, { Component, useState } from 'react';
import {
  Router,
  Switch,
  Route
} from 'react-router-dom';
import './App.css';
import history from './history';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import AppContainerElem from './Components/AppContainerElem';

import MyApp from './Views/MyApp/';
import Page1 from './Views/Page1/';
import Camera from './Views/Camera/';
import Hand from './Views/Hand/';

const Menu = {
  MenuNavBar: [
    { text: "", link: "", href: "", icon: "" }
  ],
  MenuSideBarSup: [
    { text: "Home", link: "/home", href: "", icon: "dashboard" },
    { text: "3D Render", link: "/page1", href: "", icon: "spam" },
    { text: "Camera", link: "/camera", href: "", icon: "camera" },
    { text: "Hand", link: "/hand", href: "", icon: "camera" }
  ],
  MenuSideBarInf: [
    { text: "Documentation", link: "", href: "https://www.tridyme.com/fr/documentation/fr/developpers/tridyme-webapp-kit-serverless", icon: "chrome_reader_mode" }
  ]
};

const App = () => {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const theme = React.useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: prefersDarkMode ? 'dark' : 'light',
          primary: {
            // light: will be calculated from palette.primary.main,
            main: '#000000',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
          },
          secondary: {
            //light: '#0066ff',
            main: '#ff0000',
            // dark: will be calculated from palette.secondary.main,
            //contrastText: '#ffcc00',
          },
        },
      }),
    [prefersDarkMode],
  );
  
  
  return (
    <ThemeProvider theme={theme}>
      <Router history={history}>
        <AppContainerElem
          title="My App"
          menu={Menu}
        >
          <Switch>
            <Route exact path="/" component={MyApp} />
            <Route exact path="/home" component={MyApp} />
            <Route exact path="/page1" component={Page1} />
            <Route exact path="/camera" component={Camera} />
            <Route exact path="/hand" component={Hand} />
          </Switch>
        </AppContainerElem>
      </Router>
    </ThemeProvider>
  );
};

export default App;
